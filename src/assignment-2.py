import numpy as np, pandas as pd, math, random
from sklearn.svm import LinearSVC
from sklearn.metrics import hamming_loss
from sklearn.model_selection import train_test_split

class RecurrentClassifier():

    def __init__(self, alphabet):

        self.class_expl = []                #The set of examples for classification
        self._policy = self.LearnedPolicy(self.class_expl)
        self._dummy_label = max(alphabet)
        self.alphabet = alphabet

    class Policy:
        __name__ = "Policy"

        def __init__(self, L):
            self.class_expl = L

        def decomp(self):
            X = [t[0] for t in self.class_expl]
            Y = [t[1] for t in self.class_expl]
            return X, Y

    class LearnedPolicy(Policy):
        __name__ = "Learned Policy"

        def __init__(self, L):
            super().__init__(L)
            self._classifier = LinearSVC(max_iter=10000, random_state=10)
            if len(self.class_expl) > 0: self.learn()

        def action(self, f, y_star = None):
            return self._classifier.predict([f])[0]

        def score(self, L = None):
            if L is None: L = self.class_expl
            X, Y = self.decomp()
            return self._classifier.score(X, Y)

        def learn(self, L = None):
            if L is not None: self.class_expl = L
            else: L = self.class_expl
            X, Y = self.decomp()
            self._classifier.fit(X, Y)

    class OraclePolicy(Policy):
        __name__ = "Oracle Policy"

        def __init__(self, L): super().__init__(L)

        def action(self, f, y_star = None):
            if y_star is not None: return y_star
            for lf, label in self.class_expl:                               #y_star not available at testing time
                if np.array_equal(f, lf): return label
            return None

        def score(self, L = None): return 1

    def test(self, D):
        recurrent_hamming_accuracy = 0
        oracle_hamming_accuracy = 0

        for x, y in D[:]:
            y_partial_construct = []
            y_partial_oracle = []

            T = len(y)
            for t in range(T):

                y_partial_optimal = y[:t]
                if t == 0:
                    # Handle dummy label condition
                    y_partial_construct = [self._dummy_label]
                    y_partial_oracle = [self._dummy_label]
                if t == 1:
                    # Remove dummy label
                    y_partial_construct = y_partial_construct[1:]
                    y_partial_oracle = y_partial_oracle[1:]

                f_construct = _phi(x, y_partial_construct, alphabet)
                f_optimal = _phi(x, y_partial_optimal, alphabet)

                y_hat_construct = self._policy.action(f_construct)
                y_hat_oracle = self._policy.action(f_optimal)

                y_partial_construct.append(y_hat_construct)
                y_partial_oracle.append(y_hat_oracle)
            
            recurrent_hamming_accuracy += 1.0 - hamming_loss(y, y_partial_construct, labels=self.alphabet)
            oracle_hamming_accuracy += 1.0 - hamming_loss(y, y_partial_oracle, labels=self.alphabet)

        # Compute and report accuracies
        print(" Oracle accuracy is {}%".format(round(oracle_hamming_accuracy/len(D) * 100)))
        print(" Recurrent accuracy is {}%".format(round(recurrent_hamming_accuracy/len(D) * 100)))

        return recurrent_hamming_accuracy

    def _generate_examples(self, D):
        for x, y in D[:]:
            T = len(y)
            for t in range(T):

                y_partial = y[:t]
                if t == 0: y_partial = [self._dummy_label]

                f = _phi(x, y_partial, alphabet)
                example = (f, y[t])
                self.class_expl.append(example)

class ImitationClassifier(RecurrentClassifier):
    __name__ = "Imitation Classifier"

    def __init__(self, alphabet):
        super().__init__(alphabet)

    def train(self, D, *args):

        print("Generating examples...")
        self._generate_examples(D)

        print("Training classifier...")
        self._policy.learn(self.class_expl)

        # Training accuracy
        oracle_hamming_accuracy = self._policy.score()
        print(" Done..  Oracle accuracy is {}%".format( round(oracle_hamming_accuracy * 100)))
        return oracle_hamming_accuracy

class DAggerClassifier(RecurrentClassifier):
    __name__ = "DAgger Classifier"

    def __init__(self, alphabet, beta):
        super().__init__(alphabet)
        self._default_it = 5    # Default number of DAgger iterations
        self._beta = beta       # Interpolation parameter
        self._policy = None
        self.lam = 1

    def train(self, D, *args):

        # Determine DAgger iteration count
        if len(args) > 0: d_max = args[0]
        else: d_max = self._default_it

        train_d, validation_data = train_test_split(D, test_size=0.2, random_state=10)

        print("Beta = {}".format(self._beta))

        # As if oracle classifier is giving us trajectories
        print("Generating examples...")
        self._generate_examples(train_d)

        print("Training classifier...")
        h_learned = self.LearnedPolicy(self.class_expl)
        h_oracle = self.OraclePolicy(self.class_expl)
        h_history = [] 
        recurrent_acc_history = []
        recurrent_acc_history_str = []
        beta_const = self._beta

        for j in range(d_max):
            
            self._beta = beta_const*math.exp(-self.lam*j)            #exponential decay of beta

            if random.random() < self._beta: h_current = h_oracle
            else: h_current = h_learned
            
            print("Itr", j + 1, ": Chose", h_current.__name__, len(h_current.class_expl), "with beta = {}".format(self._beta))

            for x, y in train_d:
                y_partial_construct = []

                T = len(y)
                for t in range(T):

                    y_star = y[t]

                    # Generate features
                    if t == 0: y_partial_construct = [self._dummy_label]
                    if t == 1: y_partial_construct = y_partial_construct[1:]
                    f = _phi(x, y_partial_construct, alphabet)

                    c_action = h_current.action(f, y_star)  # Predicted action
                    o_action = h_oracle.action(f, y_star)   # Oracle action

                    if c_action == o_action:
                        clf_example = (f, o_action)
                        self.class_expl.append(clf_example)

                    y_partial_construct.append(c_action)

            if not isinstance(h_current, self.OraclePolicy):
                h_history.append(h_learned)
            h_learned = self.LearnedPolicy(self.class_expl)

            recurrent_hamming_accuracy = 1.0 - hamming_loss(y, y_partial_construct, labels=self.alphabet)
            recurrent_acc_history.append(recurrent_hamming_accuracy)
            recurrent_acc_history_str.append(str(round(recurrent_hamming_accuracy*100,2)))

        print("Determining best learned policy to pick using validation data (20%)...")
        h_best_tup = (None, -1)
        for h in h_history:
            self._policy = h
            acc = self.test(validation_data)
            if acc > h_best_tup[1]: h_best_tup = (h, acc)

        if len(h_history) == 0: self._policy = self.LearnedPolicy(self.class_expl)
        else: self._policy = h_best_tup[0]
        
        # Training accuracy
        oracle_hamming_accuracy = self._policy.score()
        print(" Done..  Recurrent accuracy over {} iterations = {}".format(d_max, "%, ".join(recurrent_acc_history_str)), "%")
        print(" Done..  Oracle accuracy is {}%".format(round(oracle_hamming_accuracy * 100)))

        return

def _phi(x, y, alphabet):
    #Comment this out if you don't want to use only a small history.
    history_len = 3
    y = y[-history_len:]
    x = x[-history_len:]

    n_cls = len(alphabet)
    m = len(x[0]) # length of each token
    unary_feat = np.zeros(m*n_cls)
    binary_feat = np.zeros(n_cls**2)

    for i in range(len(y)):
        unary_feat[m*y[i] : m*(y[i]+1)] += x[i]
        if (i < len(y) - 1):
            binary_feat[y[i]*n_cls + y[i+1]] += 1
    
    return np.concatenate((unary_feat, binary_feat))

def parse_data_file(file_loc):
    train_df = pd.read_csv(file_loc, sep='\t', header=None, names=['ind', 'x', 'y', 'delim']).dropna()
    train_df['x'] = train_df['x'].str.replace('im', '')
    alphabet = []
    D = []
    x = []
    y = []

    for i in range(len(train_df)):
        #Stop reading and append word if index of previous is grater than present
        if (i > 0) and (train_df.iloc[i-1]['ind'] > train_df.iloc[i]['ind']):
                D.append((x, y))
                x = []
                y = []
        temp = []
        for j in train_df.iloc[i]['x']:
            temp.append(int(j))
        x.append(temp)
        try:
            c = int(train_df.iloc[i]['y'])          #Nettalk has integer classes
        except:
            c = ord(train_df.iloc[i]['y']) - 97     #OCR has character classes
        y.append(c)
        if c not in alphabet:
            alphabet.append(c)
    
    alphabet.sort()
    alphabet.append(max(alphabet)+1)                #This is the dummy variable.

    return D, alphabet

data_dir = "../datasets/"
raw_data = [ (data_dir + "nettalk_stress_train.txt", data_dir + "nettalk_stress_test.txt", "Dataset: Nettalk"), 
    (data_dir + "ocr_fold0_sm_train.txt", data_dir + "ocr_fold0_sm_test.txt", "Dataset: OCR")]

random.seed(10)

for raw_train, raw_test, dataset_name in raw_data:
    print("\n*********************" + dataset_name + "*********************")
    
    train_data, alphabet = parse_data_file(raw_train)
    test_data, _ = parse_data_file(raw_test)

    # Construct list of classifiers
    clfs = [ImitationClassifier(alphabet)]
    clfs += [DAggerClassifier(alphabet, beta = b / 10) for b in range(5, 6)]

    # Run through each classifier
    for c in clfs:
        print("\nClassifier = {}".format(c.__name__))
        c.train(train_data)
        print("Testing classifier...")
        acc = c.test(test_data)
