#!/bin/bash
echo "Started execution. Writing output into output.txt"
start=`date +%s`
python assignment-2.py > ../outputs/output.txt
end=`date +%s`
echo "Done.. Time taken to execute everything and write output was: $(((end-start)/60)) minutes."
